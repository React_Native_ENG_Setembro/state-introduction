import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";

export default class UserItem extends React.Component {
  render() {
    const imageAddress = this.props.message.imageUrl;
    return (
      <View style={[{
          flex: 1,
          flexDirection: this.props.fromMe ? "row-reverse" : "row" ,
          justifyContent: "space-between",
          marginBottom: 20
      }]}>
        {/* Image */}
        <View>
          <Image
            style={{ width: 80, height: 80, borderRadius: 40 }}
            source={{ uri: imageAddress }}
          />
        </View>
        
        <View style={{ 
            backgroundColor: "lightgreen", 
            width: '70%',
            borderRadius: 10,
            padding: 12
            }}>
            {/* view dos textos (nome, status...) */}
            <View>
                {/* username */}
                <Text>{ this.props.message.username }</Text>
            </View>
            
            <View>
                {/* mensagem */}
                { this.props.message.message }
            </View>
            
            <View>
                {/* data */}
                { this.props.message.date }
            </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    
  }
});
