import React from 'react';
import { StyleSheet, Text, View, ScrollView, FlatList, Button } from 'react-native';
import UserItem from './src/shared/components/user-item/user-item';

export default class App extends React.Component {
  state = {
    contador: 0,
    messages: [
      {
        message: "oi",
        username: "teste",
        imageUrl: "https://i.dailymail.co.uk/i/pix/2017/04/20/13/3F6B966D00000578-4428630-image-m-80_1492690622006.jpg"
      }
    ]
  }

  getMessageTemplate(message) {
    return (
      <UserItem message={message} />
    );
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <FlatList style={styles.container}
          data={this.state.messages}
          renderItem={({item}) => this.getMessageTemplate(item)}
          />
        <View style={{height: 50, backgroundColor: "yellow"}}>
          <Button 
            onPress={() => {
              var newMessage = {
                message: "oi",
                username: "teste",
                imageUrl: "https://i.dailymail.co.uk/i/pix/2017/04/20/13/3F6B966D00000578-4428630-image-m-80_1492690622006.jpg"
              };
              var messages = this.state.messages.concat(newMessage);
              this.setState({
                messages
              })
            }}
            title={"Add Message"}
            />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ccc',
    padding: 20,
    flexGrow: 1
  },
});
